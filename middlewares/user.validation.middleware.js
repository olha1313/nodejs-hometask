const express = require('express');
const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');

const userProps = Object.keys(user).filter((p) => p !== 'id')

const validateEmail = (email) => {
    const regex = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/;
    return regex.test(email);
}

const validatePhone = (phone) => {
    const regex = /^\+38(0\d{9})$/;
    return regex.test(phone);
}

/**
 * @param {express.Request} req;
 * @param {express.Response} res;
 * @param {express.NextFunction} next;
 */
const createUserValid = (req, res, next) => {
    const reject = (message) => {
        res.err = { status: 400, message }
        return next()
    }

    const keys = Object.keys(req.body)
    const values = Object.values(req.body)

    if (!userProps.every((p) => keys.includes(p))) {
        return reject('User entity to create is missing')
    }

    if (!keys.every((k) => userProps.includes(k))) {
        return reject('User entity to create has invalid properties')
    }

    if (!values.every((v) => typeof v === 'string')) {
        return reject('User entity to create has invalid property values')
    }

    const payload = Object.fromEntries(Object.entries(req.body).map(([k, v]) => [k, v.trim()]))
    const { email, phoneNumber, password } = payload

    if (!Object.values(payload).every((v) => v !== '')) {
        return reject('User entity to create has invalid property values')
    }

    if (!validateEmail(email)) {
        return reject('Email should be form example@gmail.com');
    }

    if (UserRepository.getOne({ email })) {
        return reject('Email already exists');
    }

    if (!validatePhone(phoneNumber)) {
        return reject('The phone number must consist of +380xxxxxxxxx');
    }

    if (UserRepository.getOne({ phoneNumber })) {
        return reject('Phone number already exists');
    }

    if (password.length <= 3) {
        return reject('Password should be greater than 3 symbols');
    }

    req.body = payload

    next();
}

/**
 * @param {express.Request} req;
 * @param {express.Response} res;
 * @param {express.NextFunction} next;
 */
const updateUserValid = (req, res, next) => {
    const reject = (message) => {
        res.err = { status: 400, message }
        return next()
    }

    const keys = Object.keys(req.body)
    const values = Object.values(req.body)

    if (!userProps.some((p) => keys.includes(p))) {
        return reject('User properties to update are missing')
    }

    if (!keys.every((p) => userProps.includes(p))) {
        return reject('User properties to update are invalid')
    }

    if (!values.every((v) => typeof v === 'string')) {
        return reject('User entity to create has invalid property values')
    }

    const payload = Object.fromEntries(Object.entries(req.body).map(([k, v]) => [k, v.trim()]))
    const { email, phoneNumber, password } = payload

    if (!Object.values(payload).every((v) => v !== '')) {
        return reject('User properties to update have invalid values')
    }

    if (email && !validateEmail(email)) {
        return reject('Email should be form example@gmail.com');
    }

    if (UserRepository.getOne({ email })) {
        return reject('Email already exists');
    }

    if (phoneNumber && !validatePhone(phoneNumber)) {
        return reject('The phone number must consist of +380xxxxxxxxx');
    }

    if (UserRepository.getOne({ phoneNumber })) {
        return reject('Phone number already exists');
    }

    if (password && password.length <= 3) {
        return reject('Password should be greater than 3 symbols');
    }

    req.body = payload

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
