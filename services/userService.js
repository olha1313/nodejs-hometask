const { UserRepository } = require('../repositories/userRepository');

class UserService {
    reject (message) {
        return { err: { message, status: 404 } }
    }

    search(id) {
        const item = UserRepository.getOne({ id });

        if (!item) {
            return this.reject('User not found');
        }

        return item;
    }

    searchAll() {
        return UserRepository.getAll();
    }

    save(data) {
        return UserRepository.create(data);
    }

    update({ id, ...data }) {
        const item = UserRepository.getOne({ id });

        if (!item) {
            return this.reject('User not found');
        }

        return UserRepository.update(id, data);
    }

    delete({ id }) {
        const item = UserRepository.getOne({ id });

        if(!item) {
            return this.reject('User not found');
        }

        return UserRepository.delete(id);
    }
}

module.exports = new UserService();
