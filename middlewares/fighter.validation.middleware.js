const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');

const fighterProps = Object.keys(fighter).filter((p) => p !== 'id')

const createFighterValid = (req, res, next) => {
    const reject = (message) => {
        res.err = { status: 400, message }
        return next()
    }

    const keys = Object.keys(req.body)
    const { name, ...stats } = req.body
    const { power, health, defense } = stats

    if (!fighterProps.every((p) => keys.includes(p))) {
        return reject('Fighter entity to create is missing')
    }

    if (!keys.every((k) => fighterProps.includes(k))) {
        return reject('Fighter entity to create has invalid properties')
    }

    if (typeof name !== 'string' || name === '') {
        return reject('Fighter entity to create has invalid properties')
    }

    if (FighterRepository.getOne({ name })) {
        return reject('Fighter with that name already exists');
    }

    if (!Object.values(stats).every((v) => typeof v === 'number')) {
        return reject('Fighter entity type to create has invalid properties')
    }

    if (health < 1 || health > 100) {
        return reject('The health of the fighter should be in the range from 1 to 100')
    }

    if (power < 1 || power > 100) {
        return reject('The power of the fighter should be in the range from 1 to 100')
    }

    if (defense < 1 || defense > 10) {
        return reject('The defense of the fighter should be in the range from 1 to 10')
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    const reject = (message) => {
        res.err = { status: 400, message }
        return next()
    }

    const keys = Object.keys(req.body)
    const { name, ...stats } = req.body
    const { power, health, defense } = stats

    if (!fighterProps.some((p) => keys.includes(p))) {
        return reject('Fighter properties to update are missing')
    }

    if (!keys.every((p) => fighterProps.includes(p))) {
        return reject('Fighter properties to update are invalid')
    }

    if (typeof name !== 'string' || name === '') {
        return reject('Fighter properties to update have invalid values')
    }

    if (FighterRepository.getOne({ name })) {
        return reject('Fighter with that name already exists');
    }

    if (!Object.values(stats).every((v) => typeof v === 'number')) {
        return reject('Fighter properties to update have invalid values')
    }

    if (health < 1 || health > 100) {
        return reject('The health of the fighter should be in the range from 1 to 100')
    }

    if (power < 1 || power > 100) {
        return reject('The power of the fighter should be in the range from 1 to 100')
    }

    if (defense < 1 || defense > 10) {
        return reject('The defense of the fighter should be in the range from 1 to 10')
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
