const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    const data = UserService.searchAll()
    Object.assign(res, { data })

    next()
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const { id } = req.params;

    const { err, ...data } = UserService.search(id)
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    if (res.err) return next();

    const { err, ...data } = UserService.save(req.body)
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    if (res.err) return next();

    const { id } = req.params;

    const { err, ...data } = UserService.update({ id, ...req.body })
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;

    const { err, ...data } = UserService.delete({ id })
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

module.exports = router;
