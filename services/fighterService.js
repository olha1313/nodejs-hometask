const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    reject (message) {
        return { err: { message, status: 404 } }
    }

    search(id) {
        const item = FighterRepository.getOne({ id });

        if (!item) {
            return this.reject('Fighter not found');
        }

        return item;
    }

    searchAll() {
        return FighterRepository.getAll();
    }

    save(data) {
        return FighterRepository.create(data);
    }

    update({ id, ...data }) {
        const item = FighterRepository.getOne({ id });

        if (!item) {
            return this.reject('Fighter not found');
        }

        return FighterRepository.update(id, data);
    }

    delete({ id }) {
        const item = FighterRepository.getOne({ id });

        if(!item) {
            return this.reject('Fighter not found');
        }

        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();
