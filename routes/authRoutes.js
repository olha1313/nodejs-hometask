const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    const { email, password } = req.body
    const user = { email, password }

    const { err, ...data } = AuthService.login(user)
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

module.exports = router;
