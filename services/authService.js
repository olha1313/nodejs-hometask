const { UserRepository } = require('../repositories/userRepository');

class AuthService {
    reject (message) {
        return { err: { message, status: 404 } }
    }

    login(userData) {
        const user = UserRepository.getOne(userData)

        if(!user) {
            return this.reject('User not found');
        }

        return user;
    }
}

module.exports = new AuthService();
