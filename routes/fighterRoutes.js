const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    const data = FighterService.searchAll()
    Object.assign(res, { data })

    next()
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const { id } = req.params;

    const { err, ...data } = FighterService.search(id)
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    if (res.err) return next();

    const { err, ...data } = FighterService.save(req.body)
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    if (res.err) return next();

    const { id } = req.params;

    const { err, ...data } = FighterService.update({ id, ...req.body })
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;

    const { err, ...data } = FighterService.delete({ id })
    Object.assign(res, { data, err })

    next()
}, responseMiddleware);

module.exports = router;
