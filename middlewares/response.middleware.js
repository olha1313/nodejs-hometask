const responseMiddleware = (req, res, next) => {

    if (res.err) {
        const { status, message } = res.err;
        res.status(status).json({ message, error: true });
    } else {
        res.status(200).json({
            message: 'Everything was successful',
            data: res.data
        });
    }

    next();
}

exports.responseMiddleware = responseMiddleware;
